const mongoose = require('mongoose');

module.exports = async () => {
    try {

        await mongoose.connect(process.env.DATABASE, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
            console.log('Connected to mongodb')
        })
        // await mongoose.connect(process.env.DATABASE);
        // console.log('DB connected successfully');
    } catch (error) {
        console.error(error);
    }
}