const express = require('express');
const router = express.Router();
const Formdata = require('../models/formdata.model');

// get a list of students from the database
router.get('/forms', async (req, res) => {
    try {
        const formdatas = await Formdata.find().sort({ updatedAt: -1 });
        res.status(200).json({ data: formdatas, success: 1, error: 0, message: 'Success' });

    } catch (err) {
        res.status(500).json(err)
    }
});

router.get('/forms/:id', async (req, res) => {
    try {
        const formdatas = await Formdata.findById(req.params.id);
        res.status(200).json({ data: formdatas, success: 1, error: 0, message: 'Success' });

    } catch (err) {
        res.status(500).json(err)
    }
});


router.post('/forms', async (req, res) => {
    try {
        const formdata = new Formdata({
            form_name: req.body.form_name,
            form_description: req.body.form_description,
            formdata: req.body.formdata,
        });
        formdata.save(function (err, result) {
            if (err) {
                res.status(200).json({ success: 0, error: 1, message: err });
            }
            else {
                res.status(200).json({ success: 1, error: 0, message: 'Form data saved!' });
            }
        })

    } catch (err) {
        res.status(500).json(err)
    }
})



// update a student in the database
router.put('/forms/:id', function (req, res, next) {
    try {
        Formdata.findOneAndUpdate({ _id: req.params.id }, req.body).then(function (result, err) {
            if (err) {
                res.status(200).json({ success: 0, error: 1, message: err });
            }
            else {
                res.status(200).json({ success: 1, error: 0, message: 'Form data updated!' });
            }
        })

    } catch (err) {
        res.status(500).json(err)
    }
   
});

// delete a student in the database
router.delete('/forms/:id', function (req, res, next) {
    try {
        Formdata.findOneAndDelete({ _id: req.params.id }).then(function (result, err) {
            if (err) {
                res.status(200).json({ success: 0, error: 1, message: err });
            }
            else {
                res.status(200).json({ success: 1, error: 0, message: 'Form data deleted!' });
            }
        })

    } catch (err) {
        res.status(500).json(err)
    }
});

module.exports = router;