const mongoose = require("mongoose");
const bools = [true, false];

const FormSchema = new mongoose.Schema(
    {
        form_name: { type: String, default: '' },
        form_description: { type: String, default: '' },
        formdata: { type: Array, default: [] }
    },
    { timestamps: true }
);

module.exports = mongoose.model("formdatas", FormSchema);