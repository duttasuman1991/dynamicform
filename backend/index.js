const { join, resolve } = require('path');
const express = require('express');
require("dotenv").config();
const http = require('http');
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const morgan = require('morgan')
const app = express();

app.use(express.json());
app.use(bodyParser.json())
app.use(morgan('common'))
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


mongoose
    .connect(process.env.DATABASE, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log("Database connected!"))
    .catch(err => console.log("Error", err));
// initialize routes

app.use('/api', require('./routes/api'));







// (async () => {
//     try {
//         await require(resolve(join(__dirname, 'database')))();

//         // const server = http.createServer(app);
//         // server.listen(process.env.PORT || 4000);
//         // server.on('error', onError);
//         // console.log(`http://localhost:${process.env.PORT || 4000}`);
//     } catch (error) {
//         console.error(error);
//     }
// })();

app.listen(process.env.PORT || 4000, function () {
    console.log(`now listening for requests port ${process.env.PORT}`);
});
