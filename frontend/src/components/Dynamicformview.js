import React, { useEffect, useRef, useState } from 'react'
import { Form } from 'react-bootstrap';


export default function Dynamicformview(props) {

    const styleref = useRef(null)
    useEffect(() => {
        if (props.item.class !== "") {
            styleref.current.className = '';
            const classArray = props.item.class.split(" ");
            classArray.forEach((classValue) => {
                if (classValue !== "") {
                    styleref.current.classList.add(classValue);
                }
            })

        }

        styleref.current.style = props.item.style;
    }, [])

    return (
        <>
            {props.item.type === 'text' && (<Form.Group className="mb-3" controlId="exampleForm.ControlInput1" key={props.index}>
                <Form.Label>Form {props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</Form.Label>
                <Form.Control type={props.item.type} placeholder="name@example.com" ref={styleref} />
            </Form.Group>)}

            {props.item.type === 'textarea' && (<Form.Group className="mb-3" controlId="exampleForm.ControlInput1" key={props.index}>
                <Form.Label>Form {props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</Form.Label>
                <Form.Control as={props.item.type} rows={3} placeholder="Textarea" ref={styleref} />
            </Form.Group>)}

            {props.item.type === 'button' && (<Form.Group className="mb-3" controlId="exampleForm.ControlInput1" key={props.index}>
                <div>
                    <button type={props.item.type} className={'btn btn-primary text-white m-2'} ref={styleref}>{props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</button>
                </div>
            </Form.Group>)}



        </>
    )
}
