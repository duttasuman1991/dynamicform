import React, { useEffect, useRef, useState } from 'react'
import { Form } from 'react-bootstrap';


export default function Dynamicform(props) {

    const styleref = useRef(null)
    const handleUpdatestyle = (event) => {
        const value = event.target.value;
        props.updateStyle(value, props.index, 'style')
        styleref.current.style = props.item.style;
    }

    const handleUpdateid = (event) => {
        const value = event.target.value;
        props.updateStyle(value, props.index, 'id')
        styleref.current.setAttribute("id", props.item.id);
        styleref.current.style = props.item.style;
    }

    const handleUpdateclass = (event) => {
        const value = event.target.value;
        props.updateStyle(value, props.index, 'class')
        styleref.current.className = '';
        const classArray = props.item.class.split(" ");
        classArray.forEach((classValue) => {
            if (classValue !== "") {
                styleref.current.classList.add(classValue);
            }
        })
        styleref.current.style = props.item.style;
    }

    const handleUpdatename = (event) => {
        const value = event.target.value;
        props.updateStyle(value, props.index, 'name')
        styleref.current.setAttribute("name", props.item.name);
        styleref.current.style = props.item.style;
    }

    useEffect(() => {
        if (props.item.class !== "") {
            styleref.current.className = '';
            const classArray = props.item.class.split(" ");
            classArray.forEach((classValue) => {
                if (classValue !== "") {
                    styleref.current.classList.add(classValue);
                }
            })

        }
        styleref.current.style = props.item.style;
    }, [])

    return (
        <>
            {props.item.type === 'text' && (<Form.Group className="mb-3" controlId="exampleForm.ControlInput1" key={props.index}>
                <Form.Label>Form {props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</Form.Label>
                <Form.Control type={props.item.type} placeholder="name@example.com" ref={styleref} />
            </Form.Group>)}

            {props.item.type === 'textarea' && (<Form.Group className="mb-3" controlId="exampleForm.ControlInput1" key={props.index}>
                <Form.Label>Form {props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</Form.Label>
                <Form.Control as={props.item.type} rows={3} placeholder="Textarea" ref={styleref} />
            </Form.Group>)}

            {props.item.type === 'button' && (<Form.Group className="mb-3" controlId="exampleForm.ControlInput1" key={props.index}>
                <Form.Label>Form {props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</Form.Label>
                <div>
                    <button type={props.item.type} className={'btn btn-primary text-white m-2'} ref={styleref}>{props.item.name.charAt(0).toUpperCase() + props.item.name.slice(1)}</button>
                </div>
            </Form.Group>)}

            <div className="d-flex border-bottom mb-3">
                <Form.Group className="mb-3 m-1">
                    <Form.Label>Id</Form.Label>
                    <Form.Control type="text" placeholder="Id" value={props.item.id} onChange={handleUpdateid} />
                </Form.Group>
                <Form.Group className="mb-3 m-1">
                    <Form.Label>Class</Form.Label>
                    <Form.Control type="text" placeholder="Class" value={props.item.class} onChange={handleUpdateclass} />
                </Form.Group>
                <Form.Group className="mb-3 m-1">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Name" value={props.item.name} onChange={handleUpdatename} />
                </Form.Group>
                <Form.Group className="mb-3 m-1">
                    <Form.Label>Style</Form.Label>
                    <Form.Control type="text" placeholder="Style" value={props.item.style} onChange={handleUpdatestyle} />
                </Form.Group>
            </div>

        </>
    )
}
