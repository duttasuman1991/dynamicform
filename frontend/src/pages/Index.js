import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { Row, Col, Table } from 'react-bootstrap';
import axios from 'axios';
import { toast } from 'react-toastify';



export default function Index(props) {

    const [formlist, setFormlist] = useState(props.formlist && props.formlist.length !== 0 ? props.formlist : [])

    useEffect(() => {
        handleLoaddata()
    }, [])

    const handleLoaddata = () => {
        axios.get(`http://localhost:3001/api/forms`)
            .then(res => {
                if (res.data.success === 1) {
                    setFormlist(res.data.data)
                } else {
                    toast.success('Server issue!')
                }
            })
    }

    const handleDelete = (id) => {
        axios.delete(`http://localhost:3001/api/forms/${id}`)
            .then(res => {
                if (res.data.success === 1) {
                    toast.success(res.data.message);
                    handleLoaddata()
                } else {
                    toast.success('Server issue!')
                }
            })
    }

    return (
        <>
            <Row className="border mt-3 p-3 rounded" data-testid="Dynamic Form List">
                <Col className="d-flex justify-content-between">
                    <h2>Dynamic Form List</h2>
                    <Link to="/add"><button type="button" className={'btn btn-info text-white'}>Add new form</button></Link>
                </Col>
            </Row>
            <Row className="mt-3 rounded">
                <Col className="p-0">
                    <Table responsive="sm" className="border">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Form name</th>
                                <th>Form Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody data-testid="tbody">
                            {formlist.map((value, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{value.form_name}</td>
                                        <td>{value.form_description}</td>
                                        <td width="15%">
                                            <div className="d-flex justify-content-end">
                                                <Link to={`/view/${value._id}`}><button type="button" className="btn btn-primary m-2">View</button></Link>
                                                <Link to={`/edit/${value._id}`}><button type="button" className="btn btn-primary m-2">Edit</button></Link>
                                                <button type="button" className="btn btn-danger m-2" onClick={() => handleDelete(value._id)}>Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}

                        </tbody>
                    </Table>
                </Col>

            </Row>

        </>
    )
}
