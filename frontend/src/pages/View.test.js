import { render, screen } from '@testing-library/react';
import { BrowserRouter } from "react-router-dom"
import View from './View';


test('Render that the View form text exist', async () => {
    render(<BrowserRouter><View /></BrowserRouter>);
    const tagElement = screen.getByTestId(/View form/i);
    expect(tagElement).toBeInTheDocument();
});