import React, { useEffect, useState } from 'react'
import { Row, Col, Form } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';

import Dynamicformview from '../components/Dynamicformview';


export default function View(props) {
    const navigate = useNavigate();
    const params = useParams();

    const [formdata, setFormdata] = useState([]);
    const [formname, setFormname] = useState("");
    const [formdescription, setFormdescription] = useState("");





    //Update style..
    const updateStyle = (value, index, attrtype) => {
        const formdatas = [...formdata];
        if (attrtype === 'style') {
            formdatas[index].style = value;
        }
        if (attrtype === 'name') {
            formdatas[index].name = value;
        }
        if (attrtype === 'class') {
            formdatas[index].class = value;
        }
        if (attrtype === 'id') {
            formdatas[index].id = value;
        }
        setFormdata(formdatas)
    }

    useEffect(() => {
        handleLoaddata()
    }, [])


    const handleLoaddata = () => {
        axios.get(`http://localhost:3001/api/forms/${params.id}`)
            .then(res => {
                if (res.data.success === 1) {
                    setFormdata(res.data.data.formdata)
                    setFormname(res.data.data.form_name)
                    setFormdescription(res.data.data.form_description)
                } else {
                    toast.success('Server issue!')
                }
            })
    }


    const handleSubmit = () => {
        console.log(formdata)
        console.log(formdescription)
        console.log(formname)

        let data = { form_name: formname, form_description: formdescription, formdata: formdata };
        axios.put(`http://localhost:3001/api/forms/${params.id}`, data)
            .then(res => {
                if (res.data.success === 1) {
                    toast.success(res.data.message);
                    navigate("../", { replace: true });
                } else {
                    toast.success('Server issue!')
                }
            })
    }




    return (
        <>
            <Row className="border mt-3 p-3 rounded">
                <Col className="d-flex justify-content-between">
                    <h2 data-testid="View form">View form</h2>
                    <button type="button" className={'btn btn-info text-white'} onClick={() => { navigate(-1) }}>Back</button>
                </Col>
            </Row>
            <Row className="mt-3 rounded border">
                <Col className="p-5">
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Form name</Form.Label>
                        <Form.Control type="text" placeholder="Form name" value={formname} onChange={(event) => { setFormname(event.target.value) }} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Form description</Form.Label>
                        <Form.Control as="textarea" rows={3} placeholder="Form description" onChange={(event) => { setFormdescription(event.target.value) }} value={formdescription} />
                    </Form.Group>
                </Col>
            </Row>

            {formdata.length > 0 &&
                <Row className="mt-3 mb-3 rounded border p-3">
                    {formdata.map((item, index) => {
                        return <Dynamicformview item={item} index={index} key={`index-${index}`} updateStyle={(value, index, attrType) => updateStyle(value, index, attrType)} />;
                    })}
                </Row>}







        </>
    )
}
