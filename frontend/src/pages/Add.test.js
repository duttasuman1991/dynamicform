import { render, screen } from '@testing-library/react';
import { BrowserRouter } from "react-router-dom"
import Add from './Add';


test('Render that the Add new form text exist', async () => {
    render(<BrowserRouter><Add /></BrowserRouter>);
    const tagElement = screen.getByTestId(/Add new form/i);
    expect(tagElement).toBeInTheDocument();
});

test('Form name field exists', async () => {
    render(<BrowserRouter><Add /></BrowserRouter>);
    const tagElement = screen.getByPlaceholderText(/Form name/i);
    expect(tagElement).toBeInTheDocument();
});

test('Form description field exists', async () => {
    render(<BrowserRouter><Add /></BrowserRouter>);
    const tagElement = screen.getByPlaceholderText(/Form description/i);
    expect(tagElement).toBeInTheDocument();
});
