import { render, screen } from '@testing-library/react';
import { BrowserRouter } from "react-router-dom"
import Index from './Index';

test('renders Dynamic Form List h2 tag in the Index page', () => {
    render(<BrowserRouter><Index /></BrowserRouter>);
    const tagElement = screen.getByTestId(/Dynamic Form List/i);
    expect(tagElement).toBeInTheDocument();
});

test('renders childs correctly', async () => {
    const formlist = [1, 2, 3, 4]
    render(<BrowserRouter><Index formlist={formlist} /></BrowserRouter>);
    const tagElement = screen.getByTestId(/tbody/i).querySelectorAll('tr').length;
    expect(tagElement).toEqual(formlist.length);

});
