import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { ToastContainer } from 'react-toastify';

import './App.css';
import Index from './pages/Index';
import Edit from './pages/Edit';
import Add from './pages/Add';
import View from './pages/View';

function App() {
  return (
    <Container>
      <div className="App" data-testid="learn react">
        <Router>
          <Routes>
            <Route exact path='/' element={<Index />}></Route>
            <Route exact path='/add' element={<Add />}></Route>
            <Route exact path='/edit/:id' element={<Edit />}></Route>
            <Route exact path='/view/:id' element={<View />}></Route>
          </Routes>
        </Router>
        <ToastContainer />
      </div>
    </Container>
  );
}

export default App;
