**##****Setup Information******


#For Backend and Frontend i have created two separate directory **"frontend"** and **"backend"**

**For Backend:**
1) go to the directory backend
2) npm install
3) npm start
4) Baseurl: http://localhost:3001/

**For Frontend:**
1) go to the directory frontend
2) npm install
3) npm start
4) Baseurl: http://localhost:3000/

**For Frontend: Unit Test**
1) npm run test


**Frontend File structure and important packages:**

1) Simply i have created a boilerplate template by using "npx create-react-app frontend"
2) For routing page i have store the pages component in the pages directory
3) For reusable component, i have stored in the component directory
4) For API call, i have used Axios package.
5) For routing, i have used react-router-dom
6) For unit testing, i have used default react-testing library.


**Backend File structure and important packages:**

1) For the backend, I have used express, mongoose, dotenv etc packages
2) For mongodb, I have used Mongodb atlas url
3) For mongodb schema, i have stored model in the "models" directory.
4) For Api routes, I have write the router code in the "routes" directory


**Frontend Functionality:**

1) In the landing page, I have listed the formlist and in the list i have shown View, Edit, Delete option
2) In the top, I have also showing the Add new form
3) After click add new form it will redirect to Add new form
4) In the Add new Form, User can create dynamic form and also change attribute details like class, id, styles, name.
5) AFter submit the form i have stored the form data in the mongodb database.
6) If user update the form updated data also updated in the database.
7) user can see the updated form preview by clicking the view Button.


**Backend Functionality:**

1) For Rest Api I have created 4 APIS for Add, Edit, Update, Delete in the api.js file.
2) For mongodb connection, i have write the code in the index.js file.













